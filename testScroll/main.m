//
//  main.m
//  testScroll
//
//  Created by Joan Francesc Guerrero Estrada on 30/4/15.
//  Copyright (c) 2015 THREE-BYTES. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
