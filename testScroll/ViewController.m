//
//  ViewController.m
//  testScroll
//
//  Created by Joan Francesc Guerrero Estrada on 30/4/15.
//  Copyright (c) 2015 THREE-BYTES. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
